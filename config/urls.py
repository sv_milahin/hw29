from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('users.urls.locations')),
    path('api/v1/', include('users.urls.users')),
    path('api/v1/', include('ads.urls.ads')),
    path('api/v1/', include('ads.urls.categorys')),
]
