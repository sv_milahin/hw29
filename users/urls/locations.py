from django.urls import path, include
from rest_framework import routers

from ..views import locations

router = routers.SimpleRouter()
router.register('location', locations.LocationViewSet, basename='location')

urlpatterns = [
    path('', include(router.urls)),
]
