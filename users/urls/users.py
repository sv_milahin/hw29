from django.urls import path


from ..views import users


urlpatterns = [
    path('user/', users.UserListView.as_view()),
    path('user/create/', users.UserCreateView.as_view()),
    path('user/<int:pk>/', users.UserDetailView.as_view()),
    path('user/<int:pk>/update/', users.UserUpdateView.as_view()),
    path('user/<int:pk>/delete/', users.UserDeleteView.as_view()),
]