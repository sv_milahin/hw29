from django.db import models

from ads.models.categorys import Category
from config.models import BaseModel
from users.models import User


class Ad(BaseModel):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    price = models.IntegerField()
    description = models.CharField(max_length=1000)
    is_published = models.BooleanField()
    image = models.ImageField(upload_to=f'images/%Y/%m/%d',
                              null=False,
                              unique=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Объявление'
        verbose_name_plural = 'Объявления'

    def __str__(self):
        return self.author.username
