from django.urls import path

from ..views import categorys

urlpatterns = [
    path('cat/', categorys.CategoryListView.as_view()),
    path('cat/<int:pk>/', categorys.CategoryDetailView.as_view()),
    path('cat/create/', categorys.CategoryCreateView.as_view()),
    path('cat/<int:pk>/update/', categorys.CategoryUpdateView.as_view()),
    path('cat/<int:pk>/delete/', categorys.CategoryDeleteView.as_view()),
]